﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Threading;
using System.Configuration;
using Microsoft.Office.Interop.Excel;

namespace Subir2
{
    public partial class Main : Form
    {
       public Main()
        {
            InitializeComponent();
        }

        //Botón Cargar
        private void btnBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog fdlg = new OpenFileDialog();
            fdlg.Title = "Buscar archivo";
            fdlg.FileName = txtFilename.Text;
            fdlg.Filter = "Archivo Excel (*.xlsx)|*.xlsx";
            fdlg.FilterIndex = 1;
            fdlg.RestoreDirectory = true;

            if (fdlg.ShowDialog() == DialogResult.OK)
            {
                txtFilename.Text = fdlg.FileName;
                using (FormWait loadPreview = new FormWait(ExcelPreview))
                {
                    loadPreview.ShowDialog(this);
                }
            }

        }

        //Sube la data en un solo ciclo
        void UploadData()
        {
            for (int i = 0; i < 1; i++)
            { 
                subiendoData();
                Thread.Sleep(2);
            }
        }

        //Vista previa de archivo Excel de SIGP antes de cargarlo
        void ExcelPreview()
        {
            try
            {
                string conexionSIGP = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source='" + txtFilename.Text + "';Extended Properties=Excel 12.0;";
                OleDbConnection origen = default(OleDbConnection);
                origen = new OleDbConnection(conexionSIGP);

                //Carga Excel en la grilla antes de importarlo
                OleDbConnection origen1 = default(OleDbConnection);
                origen1 = new OleDbConnection(conexionSIGP);

                OleDbCommand seleccion = default(OleDbCommand);
                seleccion = new OleDbCommand("select * from [PROD$]", origen1);

                OleDbDataAdapter adaptador = new OleDbDataAdapter();
                adaptador.SelectCommand = seleccion;
                DataSet ds = new DataSet();
                adaptador.Fill(ds);
                grillaExcel.Invoke((MethodInvoker)(() => grillaExcel.DataSource = ds.Tables[0]));
                btnCargar.Invoke((MethodInvoker)(() => btnCargar.Visible = true));
                origen.Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show("El formato del archivo o sus columnas no coincide con la sábana de datos SIGP."+"\n\n"+ex.Message, "Error al validar archivo", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        //Muestra cuadro de diálogo de carga
        private void btnCargar_Click(object sender, EventArgs e)
        {
            var resultado = MessageBox.Show("Está a punto de cargar datos de Producción (SIGP) en el servidor." + "\n" + "La operación podría tardar unos segundos." + "\n" + "¿Desea confirmar?", "Confirmar acción", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (resultado == DialogResult.Yes)
            {
                using (FormUploading wait = new FormUploading(UploadData))
                {
                    wait.ShowDialog(this);
                }
            } 
        }

        //Carga archivo Excel SIGP
        void subiendoData()
        {
            try
            {
                string conexion = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source='" + txtFilename.Text + "';Extended Properties=Excel 12.0;";
                OleDbConnection origen = default(OleDbConnection);
                origen = new OleDbConnection(conexion);

                OleDbCommand seleccion = default(OleDbCommand);
                seleccion = new OleDbCommand("select * from [PROD$]", origen);

                OleDbDataAdapter adaptador = new OleDbDataAdapter();
                adaptador.SelectCommand = seleccion;

                DataSet ds = new DataSet();
                adaptador.Fill(ds);

                grillaExcel.Invoke((MethodInvoker)(() => grillaExcel.DataSource = ds.Tables[0]));
                origen.Close();

                SqlConnection conexion_destino = new SqlConnection();
                conexion_destino.ConnectionString = ConfigurationManager.ConnectionStrings["Base"].ConnectionString;
                conexion_destino.Open();

                SqlBulkCopy importar = default(SqlBulkCopy);
                importar = new SqlBulkCopy(conexion_destino);
                importar.DestinationTableName = "ProduccionUploadRaw";
                importar.WriteToServer(ds.Tables[0]);
                conexion_destino.Close();
                
                using (FormUploading wait = new FormUploading(UploadData))
                {
                    wait.Close();
                }
                txtFilename.Invoke((MethodInvoker)(() => txtFilename.Text = ""));
                btnCargar.Invoke((MethodInvoker)(() => btnCargar.Visible = false));
                int rowcount = grillaExcel.RowCount - 1;
                MessageBox.Show("La sábana fue cargada correctamente." + "\n" +"Se cargaron "+rowcount+" registros.", "¡Excelente!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió un error al cargar los datos. Verifique el archivo, la conexión a la red y vuelva a intentarlo." + "\n \n" + ex.Message, "No se pudo cargar sábana", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Main_Load(object sender, EventArgs e)
        {
            // Preparando interfaz estética
            tabControl1.DrawMode = TabDrawMode.OwnerDrawFixed;
            tabControl1.SizeMode = TabSizeMode.Fixed;
            
            Size tab_size = tabControl1.ItemSize;
            tab_size.Width = 158;
            tab_size.Height += 6;
            tabControl1.ItemSize = tab_size;
        }

        private const int tab_margin = 3;

        private void tabControl1_DrawItem(object sender, DrawItemEventArgs e)
        { 
            Brush txt_brush, box_brush;
            Pen box_pen;
            System.Drawing.Rectangle tab_rect = tabControl1.GetTabRect(e.Index);

            //Fondo pestaña si está seleccionada
            if (e.State == DrawItemState.Selected)
            {
                e.Graphics.FillRectangle(Brushes.DarkGreen, tab_rect);
                e.DrawFocusRectangle();

                txt_brush = Brushes.White;
                box_brush = Brushes.Silver;
                box_pen = Pens.DarkBlue;
            }
            else
            {
                e.Graphics.FillRectangle(Brushes.LightGray, tab_rect);

                txt_brush = Brushes.Black;
                box_brush = Brushes.LightGray;
                box_pen = Pens.DarkBlue;
            }

            // Márgenes
            RectangleF layout_rect = new RectangleF(
                tab_rect.Left + tab_margin,
                tab_rect.Y + tab_margin,
                tab_rect.Width - 2 * tab_margin,
                tab_rect.Height - 2 * tab_margin);
            using (StringFormat string_format = new StringFormat())
            {

                // Draw the tab's text centered.
                using (System.Drawing.Font big_font = new System.Drawing.Font(this.Font, FontStyle.Regular))
                {
                    string_format.Alignment = StringAlignment.Center;
                    string_format.LineAlignment = StringAlignment.Center;
                    e.Graphics.DrawString(
                        tabControl1.TabPages[e.Index].Text,
                        big_font,
                        txt_brush,
                        layout_rect,
                        string_format);
                }
            }
        }

        private void btnBuscarSIGP_Click(object sender, EventArgs e)
        {
            if (dateSIGPHasta.Value < dateSIGPDesde.Value)
            {
                MessageBox.Show("La fecha de inicio (DESDE) debe ser más antigua que la fecha de fin (HASTA).", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                using (FormWait loading = new FormWait(SearchDataSIGP))
                {
                    loading.ShowDialog(this);
                }
            }         
        }

        //Busca datos en la tabla
        void SearchDataSIGP()
        {
            try
            {   //Conecta antes de buscar la data
                string strConnString = ConfigurationManager.ConnectionStrings["Base"].ConnectionString;
                using (SqlConnection con = new SqlConnection(strConnString))
                {
                    if (con.State == ConnectionState.Closed)
                    {
                        con.Open();
                    }
                    using (System.Data.DataTable dtSIGP = new System.Data.DataTable("SIGP"))
                    {
                        using (SqlCommand sqlCmd = new SqlCommand("BUSCAR_BORRAR_SABANA_SIGP @ACCION,@FEC_DESDE,@FEC_HASTA,@CHECK", con))
                        {
                            // Añade parámetros al StoredProcedure
                            sqlCmd.Parameters.AddWithValue("@ACCION", 1);
                            sqlCmd.Parameters.AddWithValue("@FEC_DESDE", dateSIGPDesde.Value);
                            sqlCmd.Parameters.AddWithValue("@FEC_HASTA", dateSIGPHasta.Value);
                            if (chkIncluirAmbasFechas.Checked == true)
                            {
                                sqlCmd.Parameters.AddWithValue("@CHECK", 1);
                            }
                            else
                            {
                                sqlCmd.Parameters.AddWithValue("@CHECK", 0);
                            }
                            // Rellena datos a la grilla de Histórico
                            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCmd);
                            sqlDataAdapter.Fill(dtSIGP);
                            dtHistorialSIGP.Invoke((MethodInvoker)(() => dtHistorialSIGP.DataSource = dtSIGP));
                            btnBuscarSIGP.Invoke((MethodInvoker)(() => btnBuscarSIGP.Enabled = true));
                            if (dtHistorialSIGP.RowCount == 0)
                            {
                                lblTotalSIGP.Invoke((MethodInvoker)(() => lblTotalSIGP.Text =$"Registros encontrados: 0"));
                                btnDeleteRecordsSIGP.Invoke((MethodInvoker)(() => btnDeleteRecordsSIGP.Enabled = false));
                            }
                            else
                            {
                                lblTotalSIGP.Invoke((MethodInvoker)(() => lblTotalSIGP.Text = $"Registros encontrados: {dtHistorialSIGP.RowCount - 1}"));
                                btnDeleteRecordsSIGP.Invoke((MethodInvoker)(() => btnDeleteRecordsSIGP.Enabled = true));
                            }
                        }
                    }
                    if (con.State == ConnectionState.Open)
                    {
                        con.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                btnBuscarSIGP.Invoke((MethodInvoker)(() => btnBuscarSIGP.Enabled = true));
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //Botón de borrado de datos de tablas
        private void btnDeleteRecordsSIGP_Click(object sender, EventArgs e)
        {
            var resultado = MessageBox.Show("Si elimina estos registros, no los podrá recuperar."+"\n"+"Tendrá que volver a cargar los datos en la base." + "\n" +"¿Desea confirmar la acción?","Confirmar Borrado", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (resultado == DialogResult.Yes)
            {
                using (FormWait loading = new FormWait(DeleteDataSIGP))
                {
                    loading.ShowDialog(this);
                }
            }
        }

        //Elimina datos en la tabla
        void DeleteDataSIGP()
        {
            try
            {   //Conecta antes de eliminar la data
                string strConnString = ConfigurationManager.ConnectionStrings["Base"].ConnectionString;
                using (SqlConnection con = new SqlConnection(strConnString))
                {
                    if (con.State == ConnectionState.Closed)
                    {
                        con.Open();
                    }
                    using (System.Data.DataTable dtSIGP = new System.Data.DataTable("SIGP"))
                    {
                        using (SqlCommand sqlCmd = new SqlCommand("BUSCAR_BORRAR_SABANA_SIGP @ACCION,@FEC_DESDE,@FEC_HASTA,@CHECK", con))
                        {
                            // Añade parámetros al StoredProcedure
                            sqlCmd.Parameters.AddWithValue("@ACCION", 2);
                            sqlCmd.Parameters.AddWithValue("@FEC_DESDE", dateSIGPDesde.Value);
                            sqlCmd.Parameters.AddWithValue("@FEC_HASTA", dateSIGPHasta.Value);
                            if (chkIncluirAmbasFechas.Checked == true)
                            {
                                sqlCmd.Parameters.AddWithValue("@CHECK", 1);
                            }
                            else
                            {
                                sqlCmd.Parameters.AddWithValue("@CHECK", 0);
                            }
                            // Una vez borrado los datos, refresca la grilla
                            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCmd);
                            sqlDataAdapter.Fill(dtSIGP);
                            dtHistorialSIGP.Invoke((MethodInvoker)(() => dtHistorialSIGP.DataSource = dtSIGP));
                            btnDeleteRecordsSIGP.Invoke((MethodInvoker)(() => btnDeleteRecordsSIGP.Enabled = false));
                            lblTotalSIGP.Invoke((MethodInvoker)(() => lblTotalSIGP.Text = "Registros encontrados: 0"));
                        }
                    }
                    if (con.State == ConnectionState.Open)
                    {
                        con.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        //Buscar Archivo TTMM
        private void btnTTMMbrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog fdlg = new OpenFileDialog();
            fdlg.Title = "Buscar archivo";
            fdlg.FileName = txtFilename.Text;
            fdlg.Filter = "Archivo Excel (*.xlsx)|*.xlsx";
            fdlg.FilterIndex = 1;
            fdlg.RestoreDirectory = true;

            if (fdlg.ShowDialog() == DialogResult.OK)
            {
                txtArchivoTTMM.Text = fdlg.FileName;
                using (FormWait loadPreview = new FormWait(ExcelPreviewTTMM))
                {
                    loadPreview.ShowDialog(this);
                }
            }
        }
        //Vista previa Excel TTMM
        void ExcelPreviewTTMM()
        {
            try
            {
                string conexionTTMM = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source='" + txtArchivoTTMM.Text + "';Extended Properties=Excel 12.0;";
                //Carga Excel en la grilla antes de importarlo
                OleDbConnection origen1 = default(OleDbConnection);
                origen1 = new OleDbConnection(conexionTTMM);

                OleDbCommand seleccion = default(OleDbCommand);
                seleccion = new OleDbCommand("select * from [Hoja1$]", origen1);

                OleDbDataAdapter adaptador = new OleDbDataAdapter();
                adaptador.SelectCommand = seleccion;
                DataSet ds = new DataSet();
                adaptador.Fill(ds);
                grillaTTMM.Invoke((MethodInvoker)(() => grillaTTMM.DataSource = ds.Tables[0]));
                btnUploadTtmm.Invoke((MethodInvoker)(() => btnUploadTtmm.Visible = true));
                origen1.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("El formato del archivo o sus columnas no coincide con la sábana de datos de Tiempos Muertos." + "\n\n" + ex.Message, "Error al validar archivo", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }



        //Cargar archivo TTMM
        private void btnUploadTtmm_Click(object sender, EventArgs e)
        {
            var resultado = MessageBox.Show("Está a punto de cargar datos de Tiempos Muertos en el servidor." + "\n" + "La operación podría tardar unos segundos." + "\n" + "¿Desea confirmar?", "Confirmar acción", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (resultado == DialogResult.Yes)
            {
                using (FormUploading wait = new FormUploading(UploadDataTTMM))
                {
                    wait.ShowDialog(this);
                }
            }
        }
        //Carga datos de Tiempos Muertos a DataBase
        void UploadDataTTMM()
        {
            try
            {
                string conexion = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source='" + txtArchivoTTMM.Text + "';Extended Properties=Excel 12.0;";
                OleDbConnection origen = default(OleDbConnection);
                origen = new OleDbConnection(conexion);

                OleDbCommand seleccion = default(OleDbCommand);
                seleccion = new OleDbCommand("select * from [Hoja1$]", origen);

                OleDbDataAdapter adaptador = new OleDbDataAdapter();
                adaptador.SelectCommand = seleccion;

                DataSet ds = new DataSet();
                adaptador.Fill(ds);

                grillaTTMM.Invoke((MethodInvoker)(() => grillaTTMM.DataSource = ds.Tables[0]));
                origen.Close();

                SqlConnection conexion_destino = new SqlConnection();
                conexion_destino.ConnectionString = ConfigurationManager.ConnectionStrings["Base"].ConnectionString;
                conexion_destino.Open();

                SqlBulkCopy importar = default(SqlBulkCopy);
                importar = new SqlBulkCopy(conexion_destino);
                importar.DestinationTableName = "ProduccionTTMMUploadRaw";
                importar.WriteToServer(ds.Tables[0]);
                conexion_destino.Close();

                using (FormUploading wait = new FormUploading(UploadData))
                {
                    wait.Close();
                }
                txtArchivoTTMM.Invoke((MethodInvoker)(() => txtArchivoTTMM.Text = ""));
                btnUploadTtmm.Invoke((MethodInvoker)(() => btnUploadTtmm.Visible = false));
                int rowcount = grillaTTMM.RowCount - 1;
                MessageBox.Show("La sábana fue cargada correctamente." + "\n" + "Se cargaron " + rowcount + " registros.", "¡Excelente!", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió un error al cargar los datos. Verifique el archivo, la conexión a la red y vuelva a intentarlo." + "\n \n" + ex.Message, "No se pudo cargar sábana", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //Acciones botón buscar TTMM
        private void btnBuscarTTMM_Click(object sender, EventArgs e)
        {
            if (dateTTMMHasta.Value < dateTTMMdesde.Value)
            {
                MessageBox.Show("La fecha de inicio (DESDE) debe ser más antigua que la fecha de fin (HASTA).", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                using (FormWait loading = new FormWait(SearchDataTTMM))
                {
                    loading.ShowDialog(this);
                }
            }
        }

        void SearchDataTTMM()
        {
            try
            {   //Conecta antes de buscar la data
                string strConnString = ConfigurationManager.ConnectionStrings["Base"].ConnectionString;
                using (SqlConnection con = new SqlConnection(strConnString))
                {
                    if (con.State == ConnectionState.Closed)
                    {
                        con.Open();
                    }
                    using (System.Data.DataTable dtTTMM = new System.Data.DataTable("dtTTMM"))
                    {
                        using (SqlCommand sqlCmd = new SqlCommand("BUSCAR_BORRAR_SABANA_TTMM @ACCION,@FEC_DESDE,@FEC_HASTA,@CHECK", con))
                        {
                            // Añade parámetros al StoredProcedure
                            sqlCmd.Parameters.AddWithValue("@ACCION", 1);
                            sqlCmd.Parameters.AddWithValue("@FEC_DESDE", dateTTMMdesde.Value);
                            sqlCmd.Parameters.AddWithValue("@FEC_HASTA", dateTTMMHasta.Value);
                            if (chkIncluirAmbasFechasTTMM.Checked == true)
                            {
                                sqlCmd.Parameters.AddWithValue("@CHECK", 1);
                            }
                            else
                            {
                                sqlCmd.Parameters.AddWithValue("@CHECK", 0);
                            }
                            // Rellena datos a la grilla de Histórico
                            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCmd);
                            sqlDataAdapter.Fill(dtTTMM);
                            dtHistorialTTMM.Invoke((MethodInvoker)(() => dtHistorialTTMM.DataSource = dtTTMM));
                            int registrostotales = dtHistorialTTMM.RowCount;
                            if (dtHistorialTTMM.RowCount == 0)
                            {
                                lblRegistrosEncontradosTTMM.Invoke((MethodInvoker)(() => lblRegistrosEncontradosTTMM.Text = "Registros encontrados: 0"));
                                btnDeleteRecordsTTMM.Invoke((MethodInvoker)(() => btnDeleteRecordsTTMM.Enabled = false));
                            }
                            else
                            {
                                btnDeleteRecordsTTMM.Invoke((MethodInvoker)(() => btnDeleteRecordsTTMM.Enabled = true));
                                lblRegistrosEncontradosTTMM.Invoke((MethodInvoker)(() => lblRegistrosEncontradosTTMM.Text = $"Registros encontrados: {dtHistorialTTMM.RowCount - 1}"));
                            }  
                        }
                    }
                    if (con.State == ConnectionState.Open)
                    {
                        con.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                btnBuscarTTMM.Invoke((MethodInvoker)(() => btnBuscarTTMM.Enabled = true));
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDeleteRecordsTTMM_Click(object sender, EventArgs e)
        {
            var resultado = MessageBox.Show("Si elimina estos registros, no los podrá recuperar." + "\n" + "Tendrá que volver a cargar los datos en la base." + "\n" + "¿Desea confirmar la acción?", "Confirmar Borrado", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (resultado == DialogResult.Yes)
            {
                using (FormWait loading = new FormWait(DeleteDataTTMM))
                {
                    loading.ShowDialog(this);
                }
            }
        }

        void DeleteDataTTMM()
        {
            try
            {   //Conecta antes de eliminar la data
                string strConnString = ConfigurationManager.ConnectionStrings["Base"].ConnectionString;
                using (SqlConnection con = new SqlConnection(strConnString))
                {
                    if (con.State == ConnectionState.Closed)
                    {
                        con.Open();
                    }
                    using (System.Data.DataTable dtTTMM = new System.Data.DataTable("TTMM"))
                    {
                        using (SqlCommand sqlCmd = new SqlCommand("BUSCAR_BORRAR_SABANA_TTMM @ACCION,@FEC_DESDE,@FEC_HASTA,@CHECK", con))
                        {
                            // Añade parámetros al StoredProcedure
                            sqlCmd.Parameters.AddWithValue("@ACCION", 2);
                            sqlCmd.Parameters.AddWithValue("@FEC_DESDE", dateTTMMdesde.Value);
                            sqlCmd.Parameters.AddWithValue("@FEC_HASTA", dateTTMMHasta.Value);
                            if (chkIncluirAmbasFechasTTMM.Checked == true)
                            {
                                sqlCmd.Parameters.AddWithValue("@CHECK", 1);
                            }
                            else
                            {
                                sqlCmd.Parameters.AddWithValue("@CHECK", 0);
                            }
                            // Una vez borrado los datos, refresca la grilla
                            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCmd);
                            sqlDataAdapter.Fill(dtTTMM);
                            dtHistorialTTMM.Invoke((MethodInvoker)(() => dtHistorialTTMM.DataSource = dtTTMM));
                            btnDeleteRecordsTTMM.Invoke((MethodInvoker)(() => btnDeleteRecordsTTMM.Enabled = false));
                            lblRegistrosEncontradosTTMM.Invoke((MethodInvoker)(() => lblRegistrosEncontradosTTMM.Text = "Registros encontrados: 0"));
                        }
                    }
                    if (con.State == ConnectionState.Open)
                    {
                        con.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        //Arrastrar y soltar
        private void grillaExcel_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.All;
        }

        //Arrastrar y soltar
        private void grillaExcel_DragDrop(object sender, DragEventArgs e)
        {
            string[] rutaArchivo = (string[])e.Data.GetData(DataFormats.FileDrop);
            txtFilename.Text = rutaArchivo[0];

            using (FormWait loadPreview = new FormWait(ExcelPreview))
            {
                loadPreview.ShowDialog(this);
            }
        }
        //Arrastrar y soltar
        private void grillaTTMM_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.All;
        }

        private void grillaTTMM_DragDrop(object sender, DragEventArgs e)
        {
            string[] rutaArchivoTTMM = (string[])e.Data.GetData(DataFormats.FileDrop);
            txtArchivoTTMM.Text = rutaArchivoTTMM[0];

            using (FormWait loadPreview = new FormWait(ExcelPreviewTTMM))
            {
                loadPreview.ShowDialog(this);
            }
        }
    }
}
