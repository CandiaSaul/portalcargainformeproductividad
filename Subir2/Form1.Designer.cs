﻿
namespace Subir2
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.grillaExcel = new System.Windows.Forms.DataGridView();
            this.txtFilename = new System.Windows.Forms.TextBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnCargar = new System.Windows.Forms.Button();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabCargarSIGP = new System.Windows.Forms.TabPage();
            this.tabHistorialSIGP = new System.Windows.Forms.TabPage();
            this.panel3 = new System.Windows.Forms.Panel();
            this.dtHistorialSIGP = new System.Windows.Forms.DataGridView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnDeleteRecordsSIGP = new System.Windows.Forms.Button();
            this.lblTotalSIGP = new System.Windows.Forms.Label();
            this.btnBuscarSIGP = new System.Windows.Forms.Button();
            this.chkIncluirAmbasFechas = new System.Windows.Forms.CheckBox();
            this.dateSIGPHasta = new System.Windows.Forms.DateTimePicker();
            this.dateSIGPDesde = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tabCargarTTMM = new System.Windows.Forms.TabPage();
            this.panel4 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.btnUploadTtmm = new System.Windows.Forms.Button();
            this.txtArchivoTTMM = new System.Windows.Forms.TextBox();
            this.btnTTMMbrowse = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.grillaTTMM = new System.Windows.Forms.DataGridView();
            this.tabHistorialTTMM = new System.Windows.Forms.TabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.dtHistorialTTMM = new System.Windows.Forms.DataGridView();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btnDeleteRecordsTTMM = new System.Windows.Forms.Button();
            this.lblRegistrosEncontradosTTMM = new System.Windows.Forms.Label();
            this.btnBuscarTTMM = new System.Windows.Forms.Button();
            this.chkIncluirAmbasFechasTTMM = new System.Windows.Forms.CheckBox();
            this.dateTTMMHasta = new System.Windows.Forms.DateTimePicker();
            this.dateTTMMdesde = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grillaExcel)).BeginInit();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabCargarSIGP.SuspendLayout();
            this.tabHistorialSIGP.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtHistorialSIGP)).BeginInit();
            this.panel2.SuspendLayout();
            this.tabCargarTTMM.SuspendLayout();
            this.panel4.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grillaTTMM)).BeginInit();
            this.tabHistorialTTMM.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtHistorialTTMM)).BeginInit();
            this.panel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // grillaExcel
            // 
            this.grillaExcel.AllowDrop = true;
            this.grillaExcel.AllowUserToAddRows = false;
            this.grillaExcel.AllowUserToDeleteRows = false;
            this.grillaExcel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grillaExcel.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grillaExcel.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.grillaExcel.Location = new System.Drawing.Point(3, 3);
            this.grillaExcel.Name = "grillaExcel";
            this.grillaExcel.ReadOnly = true;
            this.grillaExcel.RowHeadersVisible = false;
            this.grillaExcel.Size = new System.Drawing.Size(1126, 347);
            this.grillaExcel.TabIndex = 0;
            this.grillaExcel.DragDrop += new System.Windows.Forms.DragEventHandler(this.grillaExcel_DragDrop);
            this.grillaExcel.DragEnter += new System.Windows.Forms.DragEventHandler(this.grillaExcel_DragEnter);
            // 
            // txtFilename
            // 
            this.txtFilename.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtFilename.Enabled = false;
            this.txtFilename.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFilename.Location = new System.Drawing.Point(3, 22);
            this.txtFilename.Name = "txtFilename";
            this.txtFilename.Size = new System.Drawing.Size(1022, 25);
            this.txtFilename.TabIndex = 4;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tableLayoutPanel1);
            this.panel1.Controls.Add(this.grillaExcel);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1132, 482);
            this.panel1.TabIndex = 6;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 90.81272F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.187279F));
            this.tableLayoutPanel1.Controls.Add(this.btnCargar, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.txtFilename, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnBrowse, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 353);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1132, 129);
            this.tableLayoutPanel1.TabIndex = 7;
            // 
            // btnCargar
            // 
            this.btnCargar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCargar.Image = ((System.Drawing.Image)(resources.GetObject("btnCargar.Image")));
            this.btnCargar.Location = new System.Drawing.Point(1031, 52);
            this.btnCargar.Name = "btnCargar";
            this.btnCargar.Size = new System.Drawing.Size(98, 74);
            this.btnCargar.TabIndex = 2;
            this.btnCargar.Text = "Cargar";
            this.btnCargar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnCargar.UseVisualStyleBackColor = true;
            this.btnCargar.Visible = false;
            this.btnCargar.Click += new System.EventHandler(this.btnCargar_Click);
            // 
            // btnBrowse
            // 
            this.btnBrowse.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnBrowse.Image = ((System.Drawing.Image)(resources.GetObject("btnBrowse.Image")));
            this.btnBrowse.Location = new System.Drawing.Point(1031, 22);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(98, 24);
            this.btnBrowse.TabIndex = 1;
            this.btnBrowse.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 17);
            this.label1.TabIndex = 6;
            this.label1.Text = "Ubicación archivo";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabCargarSIGP);
            this.tabControl1.Controls.Add(this.tabHistorialSIGP);
            this.tabControl1.Controls.Add(this.tabCargarTTMM);
            this.tabControl1.Controls.Add(this.tabHistorialTTMM);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1146, 514);
            this.tabControl1.TabIndex = 8;
            this.tabControl1.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.tabControl1_DrawItem);
            // 
            // tabCargarSIGP
            // 
            this.tabCargarSIGP.Controls.Add(this.panel1);
            this.tabCargarSIGP.Location = new System.Drawing.Point(4, 22);
            this.tabCargarSIGP.Name = "tabCargarSIGP";
            this.tabCargarSIGP.Padding = new System.Windows.Forms.Padding(3);
            this.tabCargarSIGP.Size = new System.Drawing.Size(1138, 488);
            this.tabCargarSIGP.TabIndex = 0;
            this.tabCargarSIGP.Text = "Cargar sábana SIGP";
            this.tabCargarSIGP.UseVisualStyleBackColor = true;
            // 
            // tabHistorialSIGP
            // 
            this.tabHistorialSIGP.Controls.Add(this.panel3);
            this.tabHistorialSIGP.Controls.Add(this.panel2);
            this.tabHistorialSIGP.Location = new System.Drawing.Point(4, 22);
            this.tabHistorialSIGP.Name = "tabHistorialSIGP";
            this.tabHistorialSIGP.Padding = new System.Windows.Forms.Padding(3);
            this.tabHistorialSIGP.Size = new System.Drawing.Size(1138, 488);
            this.tabHistorialSIGP.TabIndex = 1;
            this.tabHistorialSIGP.Text = "Sábana Cargas SIGP";
            this.tabHistorialSIGP.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.dtHistorialSIGP);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(3, 77);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1132, 408);
            this.panel3.TabIndex = 1;
            // 
            // dtHistorialSIGP
            // 
            this.dtHistorialSIGP.AllowUserToAddRows = false;
            this.dtHistorialSIGP.AllowUserToDeleteRows = false;
            this.dtHistorialSIGP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtHistorialSIGP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtHistorialSIGP.Location = new System.Drawing.Point(0, 0);
            this.dtHistorialSIGP.Name = "dtHistorialSIGP";
            this.dtHistorialSIGP.ReadOnly = true;
            this.dtHistorialSIGP.RowHeadersVisible = false;
            this.dtHistorialSIGP.Size = new System.Drawing.Size(1132, 408);
            this.dtHistorialSIGP.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnDeleteRecordsSIGP);
            this.panel2.Controls.Add(this.lblTotalSIGP);
            this.panel2.Controls.Add(this.btnBuscarSIGP);
            this.panel2.Controls.Add(this.chkIncluirAmbasFechas);
            this.panel2.Controls.Add(this.dateSIGPHasta);
            this.panel2.Controls.Add(this.dateSIGPDesde);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1132, 74);
            this.panel2.TabIndex = 0;
            // 
            // btnDeleteRecordsSIGP
            // 
            this.btnDeleteRecordsSIGP.Enabled = false;
            this.btnDeleteRecordsSIGP.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteRecordsSIGP.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteRecordsSIGP.Image")));
            this.btnDeleteRecordsSIGP.Location = new System.Drawing.Point(473, 7);
            this.btnDeleteRecordsSIGP.Name = "btnDeleteRecordsSIGP";
            this.btnDeleteRecordsSIGP.Size = new System.Drawing.Size(84, 58);
            this.btnDeleteRecordsSIGP.TabIndex = 13;
            this.btnDeleteRecordsSIGP.Text = "Borrar";
            this.btnDeleteRecordsSIGP.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnDeleteRecordsSIGP.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnDeleteRecordsSIGP.UseVisualStyleBackColor = true;
            this.btnDeleteRecordsSIGP.Click += new System.EventHandler(this.btnDeleteRecordsSIGP_Click);
            // 
            // lblTotalSIGP
            // 
            this.lblTotalSIGP.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTotalSIGP.AutoSize = true;
            this.lblTotalSIGP.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalSIGP.Location = new System.Drawing.Point(563, 48);
            this.lblTotalSIGP.Name = "lblTotalSIGP";
            this.lblTotalSIGP.Size = new System.Drawing.Size(154, 17);
            this.lblTotalSIGP.TabIndex = 12;
            this.lblTotalSIGP.Text = "Registros encontrados: 0";
            // 
            // btnBuscarSIGP
            // 
            this.btnBuscarSIGP.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBuscarSIGP.Image = ((System.Drawing.Image)(resources.GetObject("btnBuscarSIGP.Image")));
            this.btnBuscarSIGP.Location = new System.Drawing.Point(384, 7);
            this.btnBuscarSIGP.Name = "btnBuscarSIGP";
            this.btnBuscarSIGP.Size = new System.Drawing.Size(84, 58);
            this.btnBuscarSIGP.TabIndex = 11;
            this.btnBuscarSIGP.Text = "Buscar";
            this.btnBuscarSIGP.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnBuscarSIGP.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnBuscarSIGP.UseVisualStyleBackColor = true;
            this.btnBuscarSIGP.Click += new System.EventHandler(this.btnBuscarSIGP_Click);
            // 
            // chkIncluirAmbasFechas
            // 
            this.chkIncluirAmbasFechas.AutoSize = true;
            this.chkIncluirAmbasFechas.Checked = true;
            this.chkIncluirAmbasFechas.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIncluirAmbasFechas.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkIncluirAmbasFechas.Location = new System.Drawing.Point(8, 46);
            this.chkIncluirAmbasFechas.Name = "chkIncluirAmbasFechas";
            this.chkIncluirAmbasFechas.Size = new System.Drawing.Size(134, 19);
            this.chkIncluirAmbasFechas.TabIndex = 10;
            this.chkIncluirAmbasFechas.Text = "Incluir ambas fechas";
            this.chkIncluirAmbasFechas.UseVisualStyleBackColor = true;
            // 
            // dateSIGPHasta
            // 
            this.dateSIGPHasta.CalendarFont = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateSIGPHasta.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateSIGPHasta.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateSIGPHasta.Location = new System.Drawing.Point(202, 17);
            this.dateSIGPHasta.Name = "dateSIGPHasta";
            this.dateSIGPHasta.Size = new System.Drawing.Size(176, 23);
            this.dateSIGPHasta.TabIndex = 9;
            // 
            // dateSIGPDesde
            // 
            this.dateSIGPDesde.CalendarFont = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateSIGPDesde.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateSIGPDesde.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateSIGPDesde.Location = new System.Drawing.Point(8, 17);
            this.dateSIGPDesde.Name = "dateSIGPDesde";
            this.dateSIGPDesde.Size = new System.Drawing.Size(176, 23);
            this.dateSIGPDesde.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(199, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 17);
            this.label2.TabIndex = 7;
            this.label2.Text = "Hasta";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "Desde";
            // 
            // tabCargarTTMM
            // 
            this.tabCargarTTMM.Controls.Add(this.panel4);
            this.tabCargarTTMM.Location = new System.Drawing.Point(4, 22);
            this.tabCargarTTMM.Name = "tabCargarTTMM";
            this.tabCargarTTMM.Padding = new System.Windows.Forms.Padding(3);
            this.tabCargarTTMM.Size = new System.Drawing.Size(1138, 488);
            this.tabCargarTTMM.TabIndex = 4;
            this.tabCargarTTMM.Text = "Cargar sábana TT.MM.";
            this.tabCargarTTMM.UseVisualStyleBackColor = true;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.tableLayoutPanel2);
            this.panel4.Controls.Add(this.grillaTTMM);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(3, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1132, 482);
            this.panel4.TabIndex = 6;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 90.81272F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.187279F));
            this.tableLayoutPanel2.Controls.Add(this.btnUploadTtmm, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.txtArchivoTTMM, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.btnTTMMbrowse, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.label4, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 353);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1132, 129);
            this.tableLayoutPanel2.TabIndex = 7;
            // 
            // btnUploadTtmm
            // 
            this.btnUploadTtmm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnUploadTtmm.Image = ((System.Drawing.Image)(resources.GetObject("btnUploadTtmm.Image")));
            this.btnUploadTtmm.Location = new System.Drawing.Point(1031, 52);
            this.btnUploadTtmm.Name = "btnUploadTtmm";
            this.btnUploadTtmm.Size = new System.Drawing.Size(98, 74);
            this.btnUploadTtmm.TabIndex = 2;
            this.btnUploadTtmm.Text = "Cargar";
            this.btnUploadTtmm.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnUploadTtmm.UseVisualStyleBackColor = true;
            this.btnUploadTtmm.Visible = false;
            this.btnUploadTtmm.Click += new System.EventHandler(this.btnUploadTtmm_Click);
            // 
            // txtArchivoTTMM
            // 
            this.txtArchivoTTMM.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtArchivoTTMM.Enabled = false;
            this.txtArchivoTTMM.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtArchivoTTMM.Location = new System.Drawing.Point(3, 22);
            this.txtArchivoTTMM.Name = "txtArchivoTTMM";
            this.txtArchivoTTMM.Size = new System.Drawing.Size(1022, 25);
            this.txtArchivoTTMM.TabIndex = 4;
            // 
            // btnTTMMbrowse
            // 
            this.btnTTMMbrowse.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnTTMMbrowse.Image = ((System.Drawing.Image)(resources.GetObject("btnTTMMbrowse.Image")));
            this.btnTTMMbrowse.Location = new System.Drawing.Point(1031, 22);
            this.btnTTMMbrowse.Name = "btnTTMMbrowse";
            this.btnTTMMbrowse.Size = new System.Drawing.Size(98, 24);
            this.btnTTMMbrowse.TabIndex = 1;
            this.btnTTMMbrowse.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnTTMMbrowse.UseVisualStyleBackColor = true;
            this.btnTTMMbrowse.Click += new System.EventHandler(this.btnTTMMbrowse_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(111, 17);
            this.label4.TabIndex = 6;
            this.label4.Text = "Ubicación archivo";
            // 
            // grillaTTMM
            // 
            this.grillaTTMM.AllowDrop = true;
            this.grillaTTMM.AllowUserToAddRows = false;
            this.grillaTTMM.AllowUserToDeleteRows = false;
            this.grillaTTMM.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grillaTTMM.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grillaTTMM.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.grillaTTMM.Location = new System.Drawing.Point(3, 3);
            this.grillaTTMM.Name = "grillaTTMM";
            this.grillaTTMM.ReadOnly = true;
            this.grillaTTMM.RowHeadersVisible = false;
            this.grillaTTMM.Size = new System.Drawing.Size(1126, 347);
            this.grillaTTMM.TabIndex = 0;
            this.grillaTTMM.DragDrop += new System.Windows.Forms.DragEventHandler(this.grillaTTMM_DragDrop);
            this.grillaTTMM.DragEnter += new System.Windows.Forms.DragEventHandler(this.grillaTTMM_DragEnter);
            // 
            // tabHistorialTTMM
            // 
            this.tabHistorialTTMM.Controls.Add(this.panel5);
            this.tabHistorialTTMM.Controls.Add(this.panel6);
            this.tabHistorialTTMM.Location = new System.Drawing.Point(4, 22);
            this.tabHistorialTTMM.Name = "tabHistorialTTMM";
            this.tabHistorialTTMM.Padding = new System.Windows.Forms.Padding(3);
            this.tabHistorialTTMM.Size = new System.Drawing.Size(1138, 488);
            this.tabHistorialTTMM.TabIndex = 5;
            this.tabHistorialTTMM.Text = "Sábana Cargas TT.MM.";
            this.tabHistorialTTMM.UseVisualStyleBackColor = true;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.dtHistorialTTMM);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(3, 77);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1132, 408);
            this.panel5.TabIndex = 1;
            // 
            // dtHistorialTTMM
            // 
            this.dtHistorialTTMM.AllowUserToAddRows = false;
            this.dtHistorialTTMM.AllowUserToDeleteRows = false;
            this.dtHistorialTTMM.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtHistorialTTMM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtHistorialTTMM.Location = new System.Drawing.Point(0, 0);
            this.dtHistorialTTMM.Name = "dtHistorialTTMM";
            this.dtHistorialTTMM.ReadOnly = true;
            this.dtHistorialTTMM.RowHeadersVisible = false;
            this.dtHistorialTTMM.Size = new System.Drawing.Size(1132, 408);
            this.dtHistorialTTMM.TabIndex = 0;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.btnDeleteRecordsTTMM);
            this.panel6.Controls.Add(this.lblRegistrosEncontradosTTMM);
            this.panel6.Controls.Add(this.btnBuscarTTMM);
            this.panel6.Controls.Add(this.chkIncluirAmbasFechasTTMM);
            this.panel6.Controls.Add(this.dateTTMMHasta);
            this.panel6.Controls.Add(this.dateTTMMdesde);
            this.panel6.Controls.Add(this.label6);
            this.panel6.Controls.Add(this.label7);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(3, 3);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(1132, 74);
            this.panel6.TabIndex = 0;
            // 
            // btnDeleteRecordsTTMM
            // 
            this.btnDeleteRecordsTTMM.Enabled = false;
            this.btnDeleteRecordsTTMM.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteRecordsTTMM.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteRecordsTTMM.Image")));
            this.btnDeleteRecordsTTMM.Location = new System.Drawing.Point(473, 7);
            this.btnDeleteRecordsTTMM.Name = "btnDeleteRecordsTTMM";
            this.btnDeleteRecordsTTMM.Size = new System.Drawing.Size(84, 58);
            this.btnDeleteRecordsTTMM.TabIndex = 13;
            this.btnDeleteRecordsTTMM.Text = "Borrar";
            this.btnDeleteRecordsTTMM.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnDeleteRecordsTTMM.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnDeleteRecordsTTMM.UseVisualStyleBackColor = true;
            this.btnDeleteRecordsTTMM.Click += new System.EventHandler(this.btnDeleteRecordsTTMM_Click);
            // 
            // lblRegistrosEncontradosTTMM
            // 
            this.lblRegistrosEncontradosTTMM.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblRegistrosEncontradosTTMM.AutoSize = true;
            this.lblRegistrosEncontradosTTMM.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRegistrosEncontradosTTMM.Location = new System.Drawing.Point(563, 48);
            this.lblRegistrosEncontradosTTMM.Name = "lblRegistrosEncontradosTTMM";
            this.lblRegistrosEncontradosTTMM.Size = new System.Drawing.Size(154, 17);
            this.lblRegistrosEncontradosTTMM.TabIndex = 12;
            this.lblRegistrosEncontradosTTMM.Text = "Registros encontrados: 0";
            // 
            // btnBuscarTTMM
            // 
            this.btnBuscarTTMM.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBuscarTTMM.Image = ((System.Drawing.Image)(resources.GetObject("btnBuscarTTMM.Image")));
            this.btnBuscarTTMM.Location = new System.Drawing.Point(384, 7);
            this.btnBuscarTTMM.Name = "btnBuscarTTMM";
            this.btnBuscarTTMM.Size = new System.Drawing.Size(84, 58);
            this.btnBuscarTTMM.TabIndex = 11;
            this.btnBuscarTTMM.Text = "Buscar";
            this.btnBuscarTTMM.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnBuscarTTMM.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnBuscarTTMM.UseVisualStyleBackColor = true;
            this.btnBuscarTTMM.Click += new System.EventHandler(this.btnBuscarTTMM_Click);
            // 
            // chkIncluirAmbasFechasTTMM
            // 
            this.chkIncluirAmbasFechasTTMM.AutoSize = true;
            this.chkIncluirAmbasFechasTTMM.Checked = true;
            this.chkIncluirAmbasFechasTTMM.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIncluirAmbasFechasTTMM.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkIncluirAmbasFechasTTMM.Location = new System.Drawing.Point(8, 46);
            this.chkIncluirAmbasFechasTTMM.Name = "chkIncluirAmbasFechasTTMM";
            this.chkIncluirAmbasFechasTTMM.Size = new System.Drawing.Size(134, 19);
            this.chkIncluirAmbasFechasTTMM.TabIndex = 10;
            this.chkIncluirAmbasFechasTTMM.Text = "Incluir ambas fechas";
            this.chkIncluirAmbasFechasTTMM.UseVisualStyleBackColor = true;
            // 
            // dateTTMMHasta
            // 
            this.dateTTMMHasta.CalendarFont = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTTMMHasta.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTTMMHasta.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTTMMHasta.Location = new System.Drawing.Point(202, 17);
            this.dateTTMMHasta.Name = "dateTTMMHasta";
            this.dateTTMMHasta.Size = new System.Drawing.Size(176, 23);
            this.dateTTMMHasta.TabIndex = 9;
            // 
            // dateTTMMdesde
            // 
            this.dateTTMMdesde.CalendarFont = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTTMMdesde.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTTMMdesde.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTTMMdesde.Location = new System.Drawing.Point(8, 17);
            this.dateTTMMdesde.Name = "dateTTMMdesde";
            this.dateTTMMdesde.Size = new System.Drawing.Size(176, 23);
            this.dateTTMMdesde.TabIndex = 8;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(199, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 17);
            this.label6.TabIndex = 7;
            this.label6.Text = "Hasta";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(45, 17);
            this.label7.TabIndex = 6;
            this.label7.Text = "Desde";
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1146, 514);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Portal de carga datos Producción";
            this.Load += new System.EventHandler(this.Main_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grillaExcel)).EndInit();
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabCargarSIGP.ResumeLayout(false);
            this.tabHistorialSIGP.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtHistorialSIGP)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tabCargarTTMM.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grillaTTMM)).EndInit();
            this.tabHistorialTTMM.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtHistorialTTMM)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView grillaExcel;
        private System.Windows.Forms.TextBox txtFilename;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabCargarSIGP;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button btnCargar;
        private System.Windows.Forms.TabPage tabHistorialSIGP;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnBuscarSIGP;
        private System.Windows.Forms.CheckBox chkIncluirAmbasFechas;
        private System.Windows.Forms.DateTimePicker dateSIGPHasta;
        private System.Windows.Forms.DateTimePicker dateSIGPDesde;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dtHistorialSIGP;
        private System.Windows.Forms.Label lblTotalSIGP;
        private System.Windows.Forms.Button btnDeleteRecordsSIGP;
        private System.Windows.Forms.TabPage tabCargarTTMM;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button btnUploadTtmm;
        private System.Windows.Forms.TextBox txtArchivoTTMM;
        private System.Windows.Forms.Button btnTTMMbrowse;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView grillaTTMM;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabHistorialTTMM;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.DataGridView dtHistorialTTMM;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button btnDeleteRecordsTTMM;
        private System.Windows.Forms.Label lblRegistrosEncontradosTTMM;
        private System.Windows.Forms.Button btnBuscarTTMM;
        private System.Windows.Forms.CheckBox chkIncluirAmbasFechasTTMM;
        private System.Windows.Forms.DateTimePicker dateTTMMHasta;
        private System.Windows.Forms.DateTimePicker dateTTMMdesde;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
    }
}

